# coding:utf-8

from django.shortcuts import render
import MySQLdb
from random import randint
from django.shortcuts import render_to_response
from django.template import RequestContext

key = ""
hashed = ""


def home(request):
    return render(request, "")


def hello(request):
    context = dict()
    context['hello'] = "Hello World!"
    return render(request, 'hello.html', context)


def index(request):
    return render_to_response("index.html", RequestContext(request))


def get_context():
    db = MySQLdb.connect("localhost", "pyselect", "pyselectpsw", "fw", charset="utf8")
    cursor = db.cursor()
    cmd = "select * from applications"
    cursor.execute(cmd)
    result = cursor.fetchall()
    app = [line[0] for line in result]
    district = [line[1] for line in result]
    ask = [line[2] for line in result]
    stat = [line[3] for line in result]

    def translate_status(st):
        if st == 0:
            return u"未审批"
        elif st == 1:
            return u"审批通过"
        elif st == 2:
            return u"审批拒绝"
        elif st == 3:
            return u"已过期"

    stat = map(translate_status, stat)
    date = [line[4] for line in result]
    count = [line[5] for line in result]
    ident = [line[6] for line in result]
    result = zip(app, district, ask, count, date, stat, ident)
    context = {
        'result': result,
        'apply': app,
        'district': district,
        'ask': ask,
        'status': stat,
        'date': date,
        'count': count,
        'ident': ident
    }
    return context


def status(request):
    context = get_context()
    return render(request, 'status.html', context)


def auth(request):
    global key
    key = str(randint(0, 1000000))
    context = {"key": key}
    return render(request, 'auth.html', context)


def console(request):
    context = get_context()
    context["psw"] = hashed
    return render(request, 'console.html', context)
