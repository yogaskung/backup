# coding:utf-8

from django.shortcuts import *
import MySQLdb
import hashlib
import view


class Apply:
    def __init__(self):
        self.apply = ""
        self.district = ""
        self.ask = ""
        self.count = 0
        self.date = ""


def receive(request):
    db = MySQLdb.connect("localhost", "pyinsert", "pyinsertpsw", "fw", charset="utf8")
    cursor = db.cursor()
    request.encoding = 'utf-8'
    _app = Apply()
    _app.apply = request.GET['apply']
    _app.ask = request.GET['ask']
    _app.count = request.GET['count']
    _app.date = request.GET['date']
    _app.district = request.GET['district']

    cmd_get_id = "select value from info where name = 'ident'"
    cursor.execute(cmd_get_id)
    ident = cursor.fetchone()[0]
    cmd_set_id = "update info set value = " + str(ident+1) + " where name = 'ident'"
    cursor.execute(cmd_set_id)
    cmd = "insert into applications (apply, district, ask, dt, cnt, status, ident) VALUES (\'" +\
          _app.apply + "\',\'" + \
          _app.district + "\',\'" + \
          _app.ask + "\',\'" + \
          _app.date + "\'," + \
          str(_app.count) + ", 0," + \
          str(ident) + ")"
    cursor.execute(cmd)
    db.commit()
    db.close()
    return HttpResponse(u"提交成功！")


def auth_login(request):
    if request.GET['password'] == hashlib.md5("mishubu" + view.key).hexdigest():
        view.hashed = hashlib.md5("mishubu" + view.key).hexdigest()
        return view.console(request)
    else:
        return HttpResponse("口令错误")


def modify(request):
    db = MySQLdb.connect("localhost", "fwadmin", "fwadminpsw", "fw", charset="utf8")
    cursor = db.cursor()
    request.encoding = 'utf-8'
    ident = request.GET['ident']
    res = request.GET['res']
    psw = request.GET['password']
    if psw == view.hashed:
        cmd = "update applications set status = "+str(res)+" where ident = \'" + ident + "\'"
        cursor.execute(cmd)
        db.commit()
        db.close()
        return view.console(request)
    else:
        return view.auth(request)


def delete(request):
    db = MySQLdb.connect("localhost", "fwadmin", "fwadminpsw", "fw", charset="utf8")
    cursor = db.cursor()
    request.encoding = 'utf-8'
    ident = request.GET['ident']
    psw = request.GET['password']
    if psw == view.hashed:
        cmd = "delete from applications where ident = \'" + ident + "\'"
        cursor.execute(cmd)
        db.commit()
        db.close()
        return view.console(request)
    else:
        return view.auth(request)


def plus_sec(request):
    db = MySQLdb.connect("localhost", "fwadmin", "fwadminpsw", "fw")
    cursor = db.cursor()
    cmd = "select value from ha where name = 'second'"
    cursor.execute(cmd)
    second = cursor.fetchone()[0]
    if not request.GET.has_key('second'):
        response = HttpResponse(str(second))
        response['Access-Control-Allow-Origin'] = '*'
        db.close()
        return response
    else:
        new = second + int(request.GET['second'])
        cmd = "update ha set value = " + str(new) + " where name = 'second'"
        cursor.execute(cmd)

        cmd = "select * from today"
        cursor.execute(cmd)
        res = cursor.fetchall()
        if len(res) < 10:
            cmd = "insert into today (player, score) values ('default player', " + request.GET['second'] + ")"
            cursor.execute(cmd)
        else:
            ordered = sorted([item[1] for item in res], reverse=True)
            if int(request.GET['second']) >= ordered[-1]:
                cmd = "update today set score = " + request.GET['second'] + " where score = " + str(ordered[-1])
                cursor.execute(cmd)

        db.commit()
        db.close()
        response = HttpResponse()
        response['Access-Control-Allow-Origin'] = '*'
        return response


def high(request):
    db = MySQLdb.connect("localhost", "fwadmin", "fwadminpsw", "fw")
    cursor = db.cursor()
    cmd = "select * from today"
    cursor.execute(cmd)
    res = cursor.fetchall()
    text = ""
    ordered = sorted([item[1] for item in res], reverse=True)
    for item in ordered:
        text += str(item) + "\n"
    response = HttpResponse(text)
    response['Access-Control-Allow-Origin'] = '*'
    return response
