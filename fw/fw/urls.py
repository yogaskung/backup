"""fw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import *
from fw.receive import *
from fw.view import *

urlpatterns = [
    url(r'^$', index),
    url(r'^hello/$', hello),
    url(r'^index/$', index),
    url(r'^status/$', status),
    url(r'^receive/$', receive),
    url(r'^auth/$', auth),
    url(r'^auth_login/$', auth_login),
    url(r'^modify/$', modify),
    url(r'^delete/$', delete),
    url(r'^ha/$', plus_sec),
    url(r'^high/$', high)
]
